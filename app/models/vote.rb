class Vote < ApplicationRecord
  belongs_to :user
  belongs_to :question, optional: true
  belongs_to :answer, optional: true

  enum selection: { negative: 0, positive: 1 }
end
